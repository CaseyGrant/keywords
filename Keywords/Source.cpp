#include <iostream>
#include <cstdlib>
#include <ctime>
#include <string>

using namespace std;

void main()
{
	string guess; // a storage spot for the user guess
	int n = 0; // allows the user to get multiple words
	int attempts = 0; // counts how many attempts the user took
	enum fields { WORD, HINT, NUM_FIELDS }; // sets up an enumerator
	const int NUM_WORDS = 10; // sets how many inputs the enum can have
	const string WORDS[NUM_WORDS][NUM_FIELDS] = // adds values to the enum
	{
		{"attack", "You do this to your enemies."}, // sets the word and hint 
		{"trip", "You can take one."},
		{"fly", "Does this word bug you?"},
		{"computer", "You're using one."},
		{"north", "Compass."},
		{"maine", "Not a lions but the state."},
		{"punctuation","It's the end of the line."},
		{"solution", "It's what your trying to find."},
		{"xylophone", "It's not a piano."},
		{"dragons", "Think mythical creatures."}
	};

	cout << "Welcome \n"; // shows text to the user
	cout << "Unscramble the word \n";
	cout << "Enter 'hint' for a hint \n";
	cout << "Enter 'quit' to quit \n";

	do // runs once more if the while is true
	{
		attempts++; // adds 1 to the attempts
		srand(static_cast<unsigned int>(time(0))); // randomizes based off of time
		int choice = (rand() % NUM_WORDS); // sets choice to a number in the enums range
		string theWord = WORDS[choice][WORD]; // sets a variable to the chosen word
		string theHint = WORDS[choice][HINT]; // sets a variable to the chosen hint

		string jumble = theWord; // sets a variable to the word so it can be jumbled
		int length = jumble.size(); // gets the length of the word
		for (int i = 0; i < length; ++i) // runs until the word is done being jumbled
		{
			int index1 = (rand() % length); // randomizes the char positions
			int index2 = (rand() % length);
			char temp = jumble[index1]; 
			jumble[index1] = jumble[index2];
			jumble[index2] = temp;
		}

		cout << "the jumbled word is: " << jumble << "\n";
		cout << "Your guess: ";
		cin >> guess; // gets the user input

		while ((guess != theWord) && (guess != "quit")) // runs until the user inputs the correct word or quits
		{
			attempts++;
			if (guess == "hint")
			{
				cout << theHint << "\n"; // displays the hint
			}
			else if (guess == "quit")
			{
				// ends the program
			}
			else
			{
				cout << "Incorrect, Try again... \n";
			}
			cout << "Your guess: ";
			cin >> guess;
		}

		if (guess == theWord)
		{
			cout << "You got it! \n";
			n++; // increases the number of correctly guessed words
		}
	}while (n != 3);

	cout << "It took you " << attempts << " attempts to get the words correct. \n";
	cout << "Thanks for playing! \n";

	do
	{
		cout << "Want to play again? \n";
		cin >> guess;
		if (guess == "yes")
		{
			system("CLS"); // clears the screen
			cout << "here we go!";
			system("pause"); // waits for a button to be pressed
			system("CLS");
			main(); // restarts the code
		}
		if (guess == "no")
		{
			cout << "bye!";
			system("pause");
		}
		if (guess != "yes" && guess != "no")
		{
			cout << "Input not valid try again. \n";
			guess = "invalid";
		}
	} while (guess == "invalid");
}

